/* -*- mode: C; buffer-read-only: t -*-
 * !!!!!!!   DO NOT EDIT THIS FILE   !!!!!!!
 * This file is built by regen/mk_invlists.pl from Unicode::UCD.
 * Any changes made here will be lost!
 */

/* This gives the number of code points that can be in the bitmap of an ANYOF
 * node.  The shift number must currently be one of: 8..12.  It can't be less
 * than 8 (256) because some code relies on it being at least that.  Above 12
 * (4096), and you start running into warnings that some data structure widths
 * have been exceeded, though the test suite as of this writing still passes
 * for up through 16, which is as high as anyone would ever want to go,
 * encompassing all of the Unicode BMP, and thus including all the economically
 * important world scripts.  At 12 most of them are: including Arabic,
 * Cyrillic, Greek, Hebrew, Indian subcontinent, Latin, and Thai; but not Han,
 * Japanese, nor Korean.  The regnode sizing data structure in regnodes.h currently
 * uses a U8, and the trie types TRIEC and AHOCORASICKC are larger than U8 for
 * shift values above 12.)  Be sure to benchmark before changing, as larger sizes
 * do significantly slow down the test suite. */

#define NUM_ANYOF_CODE_POINTS   (1 << 8)


/* More than one code point may have the same code point as their fold.  This
 * gives the maximum number in the current Unicode release.  (The folded-to
 * code point is not included in this count.)  For example, both 'S' and
 * \x{17F} fold to 's', so the number for that fold is 2.  Another way to
 * look at it is the maximum length of all the IVCF_AUX_TABLE's */
#define MAX_FOLD_FROMS 3

/* Generated from:
 * 0e8307ab7c654d9c133ea885f5413a4eb5c0123ed2178f7e1cbabed36b67792c lib/Unicode/UCD.pm
 * eb840f36e0a7446293578c684a54c6d83d249abde7bdd4dfa89794af1d7fe9e9 lib/unicore/ArabicShaping.txt
 * 333ae1e99db0504ca8a046a07dc45b5e7aa91869c685e6bf955ebe674804827a lib/unicore/BidiBrackets.txt
 * b4b9e1d87d8ea273613880de9d2b2f0b0b696244b42152bfa0a3106e7d983a20 lib/unicore/BidiMirroring.txt
 * 529dc5d0f6386d52f2f56e004bbfab48ce2d587eea9d38ba546c4052491bd820 lib/unicore/Blocks.txt
 * cdd49e55eae3bbf1f0a3f6580c974a0263cb86a6a08daa10fbf705b4808a56f7 lib/unicore/CaseFolding.txt
 * 3b019c0a33c3140cbc920c078f4f9af2680ba4f71869c8d4de5190667c70b6a3 lib/unicore/CompositionExclusions.txt
 * 7570877e0fa197c45338f7c41a02636da4e14c8dba6a3611a01cd30bf329d5ca lib/unicore/DAge.txt
 * d367290bc0867e6b484c68370530bdd1a08b6b32404601b8c7accaf83e05628d lib/unicore/DCoreProperties.txt
 * d5687a48c95c7d6e1ec59cb29c0f2e8b052018eb069a4371b7368d0561e12a29 lib/unicore/DNormalizationProps.txt
 * 743e7bc435c04ab1a8459710b1c3cad56eedced5b806b4659b6e69b85d0adf2a lib/unicore/EastAsianWidth.txt
 * f2e04bae8c856fad3a16353a99d4cc2de6c72770260379f5e4974a97548aad2a lib/unicore/EquivalentUnifiedIdeograph.txt
 * 9a3ab36d36a22bdb84de7a17b17e9b9c242134f0080f0a8b4b28d209465a8fc8 lib/unicore/HangulSyllableType.txt
 * 790bc9595795c0e0a3860a21a7f97157a134b61a4fc4ab03c7d315d07c9a6eb7 lib/unicore/IdStatus.txt
 * 71d3ed8f15cd5d8cd00cdebe62015ff26356462774b261b4a2b83d3bf46b1639 lib/unicore/IdType.txt
 * 0ce56c1294da405c0a0a0071582ac839fd229bbf97bdd260462ee571309d4ec4 lib/unicore/IndicPositionalCategory.txt
 * ffae561a51b47ddbbe267fdd8505ac3776b85b2932268809127acee84200b573 lib/unicore/IndicSyllabicCategory.txt
 * 14733bcb6731ae0c07485bf59a41cb3db08785a50bd2b46b836b4341eab7ee46 lib/unicore/Jamo.txt
 * 012bca868e2c4e59a5a10a7546baf0c6fb1b2ef458c277f054915c8a49d292bf lib/unicore/LineBreak.txt
 * 3e39509e8fae3e5d50ba73759d0b97194501d14a9c63107a6372a46b38be18e8 lib/unicore/NameAliases.txt
 * 1d5202155f14841973aa540b1625f4befbde185ac77ce5aceaaaa0501a68bd66 lib/unicore/NamedSequences.txt
 * fb9ac8cc154a80cad6caac9897af55a4e75176af6f4e2bb6edc2bf8b1d57f326 lib/unicore/NormTest.txt
 * e05c0a2811d113dae4abd832884199a3ea8d187ee1b872d8240a788a96540bfd lib/unicore/PropList.txt
 * 13a7666843abea5c6b7eb8c057c57ab9bb2ba96cfc936e204224dd67d71cafad lib/unicore/PropValueAliases.txt
 * e4935149af407fa455901832b710bccb63d2453e46d09190e234d019bcfbba45 lib/unicore/PropertyAliases.txt
 * 7e07313d9d0bee42220c476b64485995130ae30917bbcf7780b602d677d7e33f lib/unicore/ScriptExtensions.txt
 * cca85d830f46aece2e7c1459ef1249993dca8f2e46d51e869255be140d7ea4b0 lib/unicore/Scripts.txt
 * 78b29c64b5840d25c11a9f31b665ee551b8a499eca6c70d770fcad7dd710f494 lib/unicore/SpecialCasing.txt
 * 806e9aed65037197f1ec85e12be6e8cd870fc5608b4de0fffd990f689f376a73 lib/unicore/UnicodeData.txt
 * ca6d332f485a6f5f452b29b4a74146af0f2c17b7577aa4c821d597210f70611a lib/unicore/VerticalOrientation.txt
 * 0d2080d0def294a4b7660801cc03ddfe5866ff300c789c2cc1b50fd7802b2d97 lib/unicore/auxiliary/GCBTest.txt
 * 5a0f8748575432f8ff95e1dd5bfaa27bda1a844809e17d6939ee912bba6568a1 lib/unicore/auxiliary/GraphemeBreakProperty.txt
 * 371bde4052aa593b108684ae292d8ea2dbb93c19990e0cdf416fa7239557aac3 lib/unicore/auxiliary/LBTest.txt
 * f62279d8fd10935ba0cf0d8417a1dcbe7ab0d4e62f59c17e02cbe40f580c4162 lib/unicore/auxiliary/SBTest.txt
 * 61e4ba975b0a5bc1a76ee931b94914395d7289ef624e3c0d4d6b9460ee387bea lib/unicore/auxiliary/SentenceBreakProperty.txt
 * 2a676130c71194245e7c74a837e58330f202600d8ddcf4518129dd476f26e18e lib/unicore/auxiliary/WBTest.txt
 * 5188a56e91593467c2e912601ebc78750e6adc9b04541b8c5becb5441e388ce2 lib/unicore/auxiliary/WordBreakProperty.txt
 * 29071dba22c72c27783a73016afb8ffaeb025866740791f9c2d0b55cc45a3470 lib/unicore/emoji/emoji.txt
 * 4841f2090c2dbc592d3ce43bb74c2191b3da50fb9a0d00274f1448c202851b02 lib/unicore/extracted/DBidiClass.txt
 * f10a35451429137f7348825f22d624b6390c526ead3d8e756d2af9e5ed5b2b67 lib/unicore/extracted/DBinaryProperties.txt
 * ca54f6360cd288ad92113415bf1f77749015abe11cbd6798d21f7fa81f04205d lib/unicore/extracted/DCombiningClass.txt
 * db059ce45e3cec49bfda56e262fa658b3a5561b1648de266c818d2a08a85b78a lib/unicore/extracted/DDecompositionType.txt
 * d62e6950f086e53f47c593a38342621f8838f48c49a1de070cf83d3959bd1688 lib/unicore/extracted/DEastAsianWidth.txt
 * fe29a45c0882500e591140aaa5c4f5067e6a5d746806148af34400c48b9c06f9 lib/unicore/extracted/DGeneralCategory.txt
 * e13ca1344b16023aa38c6ada39f9658536fc6bb7c3c24d579f0bc316a4f4f1e0 lib/unicore/extracted/DJoinGroup.txt
 * c4870b11e2b8b7d0eb70b99ce85608e5c28a399efa316cca97238a58ae160e5e lib/unicore/extracted/DJoinType.txt
 * 3f4f32ed2a577344a508114527e721d7a8b633d32f38945d47fe0c743650c585 lib/unicore/extracted/DLineBreak.txt
 * 710abf2d581ac9c57f244c0834f9d9969d9781e0396adccd330eaae658ac7d6b lib/unicore/extracted/DNumType.txt
 * 6bd30f385f3baf3ab5d5308c111a81de87bea5f494ba0ba69e8ab45263b8c34d lib/unicore/extracted/DNumValues.txt
 * 8968a5ee00063fa31ff15474b95ccb6b9c228e3ebad6c20bc77f4225187e2023 lib/unicore/mktables
 * 55d90fdc3f902e5c0b16b3378f9eaa36e970a1c09723c33de7d47d0370044012 lib/unicore/version
 * 0a6b5ab33bb1026531f816efe81aea1a8ffcd34a27cbea37dd6a70a63d73c844 regen/charset_translations.pl
 * c7ff8e0d207d3538c7feb4a1a152b159e5e902d20293b303569ea8323e84633e regen/mk_PL_charclass.pl
 * 0c9dc59e0c05912141ae41bce0bc55e48cae956407b7a1f12807a3d7ffea3b19 regen/mk_invlists.pl
 * ex: set ro ft=c: */
